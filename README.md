![scene pic](docs/figures/scene_view.jpg)

# demo_bachelor_scene
Репозиторий со сценой для demo версии олимпиады бакалавров.  

Для локальной сборки достаточно выполнить, не забыв выполнить при этом `docker login` в gitlab.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

```bash
docker build --pull -t scene_bachelor_demo . --build-arg  BASE_IMG=registry.gitlab.com/beerlab/iprofi2023/demo/bachelor/base:nvidia-latest
```
Для дальнейшего запуска дадим возможность авторизации для X-server извне:
```bash
xhost +local:docker
```

Далее для запуска достаточно склонировать репозиторий, предоставляемый участникам https://gitlab.com/beerlab/iprofi2023/demo/bachelor и запустить:
```bash
docker compose -f docker-compose.nvidia.yml up
```
Или без docker compose:
```bash
docker run -ti scene_bachelor_demo:latest
```

Открыть новую bash-сессию в контейнере

```bash
docker exec -ti scene bash
```

## License
MIT License
