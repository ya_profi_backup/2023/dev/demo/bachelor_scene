ARG BASE_IMG

FROM ${BASE_IMG}
LABEL org.opencontainers.image.authors="texnoman@itmo.com"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

ENV GUI false

RUN apt-get update

RUN cd src && git clone https://github.com/be2rlab/rbcar_sim.git\
    && git clone https://github.com/be2rlab/robotnik_sensors.git\ 
    && git clone https://github.com/be2rlab/rbcar_common.git\
    && git clone https://github.com/RobotnikAutomation/axis_camera_ptz\ 
    && git clone https://github.com/RobotnikAutomation/robot_localization_utils.git\
    && git clone https://github.com/RobotnikAutomation/joint_read_command_controller.git

RUN apt install -y ros-noetic-velodyne-gazebo-plugins ros-noetic-velodyne 


RUN cd src/rbcar_common/rbcar_control/lib/ &&sudo apt install -y ./* && sudo apt --fix-broken install -y ./*

COPY . src/bachelor_demo_scene

RUN catkin build

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
ENTRYPOINT ["/bin/bash", "-ci", "roslaunch bachelor_demo_scene start_scene.launch use_gui:=${GUI}"]